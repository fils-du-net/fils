You can propose *pull requests* to correct or improve the website itself.

This can be about the theme, or a script used to manage the press review, for a newsletter for example.