#!/bin/bash

href=$1
dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd .. && pwd)"

# Go into working directory
mkdir -p /tmp/fils && cd /tmp/fils || exit

# Download original article
rm article.html
wget -cN --convert-links "$href" -O article.html

function extract() {
	# This is the destination variable
	declare -n ret=$1
	# Get the related pattern
	declare -n pattern=$1_pattern

	# Get out if variable already filled
	[[ $ret ]] && return 0

	ret="$(sed -nr "s|^\\s*$pattern$|\\1|p" article.html | head -n 1)"
	[[ ! $ret ]] && ret="$(sed -nr "s|^\\s$pattern.*|\\1|p" article.html | head -n 1)"
	[[ ! $ret ]] && ret="$(sed -nr "s|.*$pattern.*|\\1|p" article.html | head -n 1)"

	[[ ! $ret ]] && ret="$(sed -nr "s|.*${default_patterns[$1]}.*|\\1|p" article.html | head -n 1)"

	if [[ $ret ]]
	then
		# Replacing some characters
		ret=${ret//&#039;/\'}
		ret=${ret//&quot;/\'}

		# Deleting extra spaces
		ret=${ret//&nbsp;}
		ret=${ret// / }
		ret=${ret//« /«}
		ret=${ret// »/»}
		ret=${ret// \:/:}
		ret=${ret// \!/!}
		ret=${ret// \?/?}
		ret=${ret//’/\'}
		ret=${ret%%\"/>*}
		ret=${ret%%/>*}

		if [[ $ret =~ "&#" || $ret =~ "eacute" || $ret =~ "egrave" ]]
		then
			ret=$(echo "$ret" | recode html)
			if [[ $ret =~ "&#" || $ret =~ "eacute" || $ret =~ "egrave" ]]
			then
				ret=$(echo "$ret" | recode html)
			fi
		fi

		if [[ ${ret,,} =~ "la rédaction" ]]
		then
			## Reset this variable
			ret=
		fi
		echo "$1: $ret"
	else
		echo "Nothing found for \"$1\" with pattern: $pattern, or default pattern: ${default_patterns[$1]}"
	fi
}

# Declaring variables that extractions will output to
site=
title=
author=
date=
featured_image=
abstract=

declare -A default_patterns=(
	[site]='<meta property="og:site_name" content="(.*)".*>'
	[title]='<title.*>\s*(.*)\s*</title>'
	[author]='"author":\{"@type":"Person","name":"(.*)"\}'
	[date]='"(20[0-9-]*)T'
	[featured_image]='src=.(http.[?&-_\.:/a-zA-Z0-9]*\....)"'
	[abstract]='<meta name=".escription" content="(.*)".*>'
)

# shellcheck source=patterns.sh
source "$dir/bin/patterns.sh" "$href"

extract site
extract title
extract author
extract date
extract featured_image
extract abstract

featured_image=${featured_image//http/featured_image: http}
# Replace eventual \\/ into simpler /
featured_image=${featured_image//\\\//\/}

[[ -z $title ]] && echo "No title found, please fill it: " && read -r title

# Remove eventual site name from title
[[ $site ]] && title=$(echo "$title" | sed -r "s/\\s*. $site//gi")

[[ $date ]] && date=${date%%T*} && date=$(echo "$date" | sed -r "s/\\b./\\L&/g" | head -n 1)
date=${date% *}
[[ $date == '' ]] && echo "No date found, which one to use: " && read -r date
[[ $date == '' ]] && echo 'Sorry, a date is required!!'
if [[ ${date% *} =~ ../../.... ]]
then
	year=${date##*/}
	day=${date%%/*}
	month_day=${date#*/}
	month=${month_day%/*}
	date=$year-$month-$day
fi

[[ $date ]] && date2=$(date -d "$date" +'%Y-%m-%d')
[[ $date2 ]] || date2=$(date -d "$date" +'%Y-%m-%d')
[[ $date2 ]] && date=$(date -d "$date" +'%A %_d %B %Y')

declare -a _tags=(
	april
	Associations
	"Brevets logiciels"
	"Vente liée"
	Informatique-deloyale
	DRM
	Entreprise
	Internet
	"Logiciels privateurs"
	"Neutralité du Net"
	"Marchés publics"
	Institutions
	HADOPI
	DADVSI
	"Administrations et collectivités"
	Économie
	Interopérabilité
	"Open Data"
	"Partage du savoir"
	"Matériel libre"
	Sensibilisation
	Promotion
	Accessibilité
	Désinformation
	"Droit d'auteur"
	Éducation
	Innovation
	Référentiel
	Sciences
	Standards
	"Vote électronique"
	"Informatique en nuage"
	"Contenus libres"
	Europe
	International
	ACTA
	English
	Licenses
	"Philosophie GNU"
	"Vie privée"
)

tags=''
for tag in "${!_tags[@]}"
do
	if grep -qoi "${_tags[tag]}" article.html
	then
		tags="$tags
- ${_tags[tag]}"
	fi
done

# Create the file
year=$(date '+%Y' -d "$date2")
month=$(date '+%m' -d "$date2")
mkdir -p "$dir/content/article/$year/$month"
# Replace spaces with dashes
name=${title//[  ]/-}
# Only keep alphanumeric characters, and dashes
name=${name//[^[:alnum:]-]/}

filename=$dir/content/article/$year/$month/$(date '+%d' -d "$date2")-$name.md

echo "---
site: ${site//\"}
title: \"${title//\"/\\\"}\"
author: $author
date: $date2
href: $href
$featured_image
tags:$tags
series:
- $(date -d "$date2" '+%Y%V')
---
" | sed -e '/^\w*: $/d' > "$filename" # Suppress empty elements

[[ $abstract ]] && printf "> %s" "$abstract" >> "$filename"
