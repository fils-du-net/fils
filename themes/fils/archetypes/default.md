---
site:
title: "{{ replace .TranslationBaseName "-" " " | title }}"
author:
date: {{ .Date }}
href:
featured_image:
description:
tags:
series:
draft: true
---

