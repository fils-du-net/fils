---
                  _____ _ _           _         _   _      _   
                 |  ___(_) |___    __| |_   _  | \ | | ___| |_ 
                 | |_  | | / __|  / _` | | | | |  \| |/ _ \ __|
                 |  _| | | \__ \ | (_| | |_| | | |\  |  __/ |_ 
                 |_|   |_|_|___/  \__,_|\__,_| |_| \_|\___|\__|
---

# Untangling the web's threads

This is a static website listing all articles pertaining to a subject of your choice. Its initial target is Free Software, particularly related to the [april](https://www.april.org) association.

It is technically based on [**hugo**](https://gohugo.io), a static website generator that manages the presentation. Articles are stored as basic files, in the [markdown](https://daringfireball.net/projects/markdown) format.

*No* relational database, *no* application server, *just* a **web server** like apache or nginx and the **file system**.

It is also possible and recommended to use [git](https://git-scm.com) in order to backup the website and the articles, and to share them among press reviews. This can be organised into a decentralised community.

See the wiki for more information on installation and usage.


Note that "Fils du Net" is also an experiment into a new kind of web applications, using command line tools, git cherry picking, gitlab.

It is thus very much a work in progress, and not anybody would be able to use it, yet...

In time, some kind of web frontend will be setup to propose and manage the articles. Possibly something called [simpleWeb](https://simpleweb.acoeuro.com).

## Some notable features

Highly **performant** and **secure** website, using sturdy technologies, to the point of brutality :)

A setup which can be organised in many different ways, and will *adapt* to many different uses. It will still need (for the time being) some technical capacities, into git and markdown.

Some independent tools can be used to import list of URLs, create a markdown article from an URL, periodically send series of articles by mail, share articles on externals sites like mastodon, diaspora, twitter, facebook, etc.

See the wiki for more.
